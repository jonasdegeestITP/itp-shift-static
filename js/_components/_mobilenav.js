'use strict';

var jsMobileNav = {
  navigation: $('.js-nav-mobile'),
  trigger: $('.js-nav-trigger'),

  init: function () {
    jsMobileNav.initDependencies();
  },

  initDependencies: function () {
    jsMobileNav.trigger.on('click', function () {
      $('body').toggleClass('has-open-nav');
    });
  }
};
